<?php

if (($_SERVER['REMOTE_ADDR'] != '31.13.133.138' 
    && $_SERVER['REMOTE_ADDR'] != '46.147.233.74'
    && $_SERVER['REMOTE_ADDR'] != '172.21.0.1')
    ) {
    die;
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

date_default_timezone_set('Europe/Moscow');

define('CONFIGS_FOLDER', dirname(__DIR__) . '/config/');

/** @var array $settings Instantiate the app */
$settings = require CONFIGS_FOLDER . 'settings.php';

/** @var DI\Container $dependencies Set up dependencies */
$dependencies = require CONFIGS_FOLDER . 'dependencies.php';
$dependencies->set('settings', $settings);

/** @var Closure $routes Register routes function */
$routes = require CONFIGS_FOLDER . 'routes.php';

// Run app
\Slim\Factory\AppFactory::setContainer($dependencies);

$app = \Slim\Factory\AppFactory::create();

$routes($app);

$errorMiddleware = $app->addErrorMiddleware($settings['displayErrorDetails'], true, true, $dependencies->get('logger'));

// Set the Not Found Handler
$errorMiddleware->setErrorHandler(
    HttpNotFoundException::class,
    function (\Psr\Http\Message\ServerRequestInterface $request, Throwable $exception, bool $displayErrorDetails) {
       
        $response = new \GuzzleHttp\Psr7\Response();
        $response->getBody()->write('404 NOT FOUND');

        return $response->withStatus(404);
    });

// // Set the Not Allowed Handler
$errorMiddleware->setErrorHandler(
    HttpMethodNotAllowedException::class,
    function (\Psr\Http\Message\ServerRequestInterface $request, Throwable $exception, bool $displayErrorDetails) {
        $response = new \GuzzleHttp\Psr7\Response();
        $response->getBody()->write('405 NOT ALLOWED');

        return $response->withStatus(405);
    });

$app->run();