<?php

use Monolog\Logger;

return [
    'debug' => false,
    'displayErrorDetails' => true, # set to false in production
    'addContentLengthHeader' => false, # Allow the web server to send the content-length header

    # Renderer settings
    'renderer' => [
        'template_path' => __DIR__ . '/../templates/',
    ],

    # Monolog settings
    'logger' => [
        'name' => 'thsome-app', # rename if u need
        'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/runtime.log',
        'level' => Logger::DEBUG,
    ],

    'Bitrix24' => require __DIR__ . '/third-party/bitrix24.conf.prod.php',

    # Cache settings
    'cache' => [
        'ttl' => 60 * 15 # cache dies in 15 minute
    ],
];
