<?php

return [
    'subdomain' => 'subdomain', # example: 'subdomain.bitrix24.ru' in this here should be 'subdomain'
    'userId' => '0', # user that created webhook
    'hook' => 'webhook', # webhook string
];