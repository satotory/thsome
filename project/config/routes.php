<?php declare(strict_types=1);

use Slim\App;
use App\Slim\Router;

/**
 * @param App $app
 */
return function (App $app) {
    $app->get('/', Router::EMPLOYERS_ACTION);

    $app->get('/{' . Router::QUERY_PARAM_EMPLOYEE_ID. '}/tasks/', Router::TASKS_ACTION);
    $app->get('/{' . Router::QUERY_PARAM_EMPLOYEE_ID. '}/tasks', Router::TASKS_ACTION);

    $app->get(
        '/{' . Router::QUERY_PARAM_EMPLOYEE_ID. '}/tasks/{' . Router::QUERY_PARAM_TASK_ID . '}/comments/',
        Router::COMMENTS_ACTION
    );
    $app->get(
        '/{' . Router::QUERY_PARAM_EMPLOYEE_ID. '}/tasks/{' . Router::QUERY_PARAM_TASK_ID . '}/comments', 
        Router::COMMENTS_ACTION
    );
};
