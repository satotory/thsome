<?php declare(strict_types=1);

use DI\Container;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\HandlerStack;

use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Storage\Psr16CacheStorage;
use Kevinrob\GuzzleCache\Strategy\GreedyCacheStrategy;
use Kodus\Cache\FileCache;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;

use Psr\Container\ContainerInterface;
use Psr\SimpleCache\CacheInterface;

use Slim\Views\PhpRenderer;

use App\Slim\Controllers\EmployersController;
use App\Bitrix24Client\Client as Bitrix24Client;
use App\Slim\Controllers\TasksController;
use App\Slim\Controllers\CommentsController;

$container = new Container();

// view renderer
$container->set('renderer', function (ContainerInterface $c) {
    $settings = $c->get('settings');

    $render = new PhpRenderer($settings['renderer']['template_path'], []);

    $render->setLayout("layout.phtml");

    return $render;
});

// monolog
$container->set('logger', function (ContainerInterface $c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Logger($settings['name']);
    $logger->pushProcessor(new UidProcessor());
    $logger->pushHandler(new StreamHandler($settings['path'], $settings['level']));
    return $logger;
});

$container->set('httpClient', function (ContainerInterface $c) {
    /** @var CacheInterface $cache */
    $cache = $c->get('psrCache');
    $ttl = $c->get('settings')['cache']['ttl'];

    $psrCache = new Psr16CacheStorage($cache);
    $cacheStrategy = new GreedyCacheStrategy($psrCache, $ttl); // 24h

    // Create default HandlerStack
    $stack = HandlerStack::create();
    // Add this middleware to the top with `push`
    $stack->push(new CacheMiddleware($cacheStrategy), 'cache');

    // Initialize the client with the handler option
    return new HttpClient(['handler' => $stack]);
});

$container->set('bitrix24Client', function (ContainerInterface $c) {
    return new Bitrix24Client(
        $c->get('settings')['Bitrix24']['subdomain'],
        $c->get('settings')['Bitrix24']['userId'],
        $c->get('settings')['Bitrix24']['hook'],
        [],
        $c->get('httpClient')
    );
});

$container->set('psrCache', function (ContainerInterface $c) {
    $ttl = $c->get('settings')['cache']['ttl'];

    return new FileCache(__DIR__ . '/../cache', $ttl);
});

$container->set('template', function () {

});

$container->set(EmployersController::class, function (ContainerInterface $c) {
    return new EmployersController($c->get('renderer'), $c->get('bitrix24Client'));
});

$container->set(TasksController::class, function (ContainerInterface $c) {
    return new TasksController($c->get('renderer'), $c->get('bitrix24Client'));
});

$container->set(CommentsController::class, function (ContainerInterface $c) {
    return new CommentsController($c->get('renderer'), $c->get('bitrix24Client'));
});

return $container;