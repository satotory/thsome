<?php declare(strict_types=1);

namespace App\Bitrix24Client\Models;

class Task
{
    public $id;
    public $name;
    public $description;

    /**
     * @param $array
     * @return Task
     */
    public static function fromArray($array)
    {
        $obj =  new Task();
        // echo "<pre>";
        // var_dump($array);
        // echo "</pre>";        
        $obj->id   = $array['id'];
        $obj->name = $array['title'];

        return $obj;
    }
}
