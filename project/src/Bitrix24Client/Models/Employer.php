<?php declare(strict_types=1);

namespace App\Bitrix24Client\Models;

class Employer
{
    /** @var string */
    public $id;
    
    /** @var string */
    public $name;

    /** @var string */
    public $lastName;

    /** @var string */
    public $email;

    /** @var string */
    public $photo;

    /** @var string */
    public $workPosition;

    /** @var bool */
    public $online;

    /**
     * @param $array
     * @return Employer
     */
    public static function fromArray($array)
    {
        $obj =  new Employer();

        $obj->id   = $array['ID'];
        $obj->name = $array['NAME'];
        $obj->lastName = $array['LAST_NAME'];
        $obj->email = $array['EMAIL'];
        $obj->photo = $array['WORK_POSITION'];
        $obj->workPosition = $array['WORK_POSITION'];
        $obj->online = $array['IS_ONLINE'] == 'Y';

        return $obj;
    }
}
