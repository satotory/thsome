<?php declare(strict_types=1);

namespace App\Bitrix24Client\Models;

class Comment
{
    public $id;
    public $message;
    public $authorId;
    public $authorName;
    public $date;

    /**
     * @param $array
     * @return Comment
     */
    public static function fromArray($array)
    {
        $obj = new Comment();

        $obj->id = $array['ID'];
        $obj->authorId = $array['AUTHOR_ID'];
        $obj->authorName = $array['AUTHOR_NAME'];
        $obj->message = self::decodeBBMessage($array['POST_MESSAGE']);
        $obj->date = self::beautifyDate($array['POST_DATE']);

        return $obj;
    }

    private static function beautifyDate(string $uglyDate)
    {
        $old_date_timestamp = strtotime($uglyDate);
        
        return date('G:i, j F', $old_date_timestamp);  
    }

    private static function decodeBBMessage(string $str)
    {
        $bbcode = [
            "/\[USER=.*?](.*?)\[\/USER]/is" => "<b>$1</b>",
            "/\<br \/>/is" => "",
            "/\[URL=(.*?)](.*?)\[\/URL]/is" => "<a href='$1'>$2</a>",
            "/\[QUOTE](.*?)\[\/QUOTE]/is" => "<p class='ps-3'><em>$1</em></p>",
            "/\[DISK FILE.*?]/is" => "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-card-image' viewBox='0 0 16 16'><path d='M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z'/><path d='M1.5 2A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13zm13 1a.5.5 0 0 1 .5.5v6l-3.775-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12v.54A.505.505 0 0 1 1 12.5v-9a.5.5 0 0 1 .5-.5h13z'/></svg>",
        ];

        $str = preg_replace(array_keys($bbcode), array_values($bbcode), $str);

        return $str;
    }
}