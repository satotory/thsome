<?php declare(strict_types=1);

namespace App\Bitrix24Client;

use App\Bitrix24Client\Collections\CommentsCollection;
use App\Bitrix24Client\Collections\TasksCollection;
use App\Bitrix24Client\Models\Comment;
use App\Bitrix24Client\Models\Employer;
use App\Bitrix24Client\Collections\EmployersCollection;
use App\Bitrix24Client\Models\Task;
use Closure;
use Exception;
use GuzzleHttp\ClientInterface;

class Client
{
    const URI_TEMPLATE = 'https://%s.bitrix24.ru/rest/%s/%s/';

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var array
     */
    private $options;

    /**
     * @param string          $apiKey
     * @param array           $options
     * @param ClientInterface $httpClient
     */
    public function __construct(
        string $subdomain,
        string $userId,
        string $hook,
        array $options,
        ClientInterface $httpClient
    ) {
        $this->options = $options;

        if (!isset($this->options['base_uri'])) {
            $this->options['base_uri'] = sprintf(
                self::URI_TEMPLATE,
                $subdomain,
                $userId,
                $hook
            );
        }
        
        $this->options['subdomain'] = $subdomain;

        $this->options['time'] = [
            'start' => date(
                DATE_ATOM,
                mktime(
                    0,
                    00,
                    00,
                    (int) date("n"),
                    (int) date("d"),
                    (int) date("y")
                )
            ),
            'end' => date(
                DATE_ATOM,
                mktime(
                    23,
                    59,
                    59,
                    (int) date("n"),
                    (int) date("d"),
                    (int) date("y")
                )
            ),
        ];

        $this->client = $httpClient;
    }

    /**
     * @return EmployersCollection|Employer[]
     * @throws ClientException
     */
    public function getEmployers(): EmployersCollection
    {
        $path = 'user.get';
        $params = [
            "filter" => [
                "ACTIVE" => "true",
                "WORK_POSITION" => ["технолог", "технический директор", "ведущий технолог"],
            ],
        ];

        $arrayToModel = function ($array) {
            return new EmployersCollection($array);
        };

        return $this->httpRequest($path, $params, $arrayToModel);
    }

    /**
     * @return TasksCollection|Task[]
     * @throws ClientException
     */
    public function getTasks(string $employeeId): TasksCollection
    {
        $path = 'tasks.task.list';
        $params = [
            "filter" => [
                "RESPONSIBLE_ID" => $employeeId,
                "<=ACTIVITY_DATE" => $this->options['time']['end'],
                ">=ACTIVITY_DATE" => $this->options['time']['start'],
            ],
            "order" => [
                "ACTIVITY_DATE" => "desc",
            ],
            "select" => [
                "ID",
                "CREATED_DATE",
                "CHANGED_DATE",
                "CLOSED_DATE",
                "DEADLINE",
                "ACTIVITY_DATE",
                "RESPONSIBLE_ID",
                "TITLE",
                "STATUS",
                "AUDITORS",
                "ACCOMPLICES",
            ],
            "start" => 0,
        ];

        $arrayToModel = function ($array) {
            return new TasksCollection($array);
        };

        return $this->httpRequest($path, $params, $arrayToModel);
    }

    /**
     * @return CommentsCollection|Comment[]
     * @throws ClientException
     */
    public function getComments(string $taskId): CommentsCollection
    {
        $path = 'task.commentitem.getlist';
        $params = [
            "taskid" => $taskId,
            "order" => [
                "POST_DATE" => "asc",
            ],
            "filter" => [
                "<=POST_DATE" => $this->options['time']['end'],
                ">=POST_DATE" => $this->options['time']['start'],
            ],
        ];

        $arrayToModel = function ($array) {
            return new CommentsCollection($array);
        };

        return $this->httpRequest($path, $params, $arrayToModel);
    }

    /**
     * @param string  $path
     * @param array   $params
     *
     * @param Closure $closure
     *
     * @return mixed
     * @throws ClientException
     */
    private function httpRequest(string $path, array $params, Closure $closure)
    {
        $options = $this->getOptions();
        $options['query'] = $params;

        try {
            /** @var \GuzzleHttp\Client */
            $client = $this->getClient();
            $response = $client->get($path, $options);
            $body = $response->getBody()->getContents();
            $array = \GuzzleHttp\json_decode($body, true);
        } catch (Exception $e) {
            throw new ClientException($e->getMessage(), $e->getCode(), $e);
        }

        return $closure($array, $response);
    }

    private function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function getOptions(): array
    {
        return $this->options;
    }
}