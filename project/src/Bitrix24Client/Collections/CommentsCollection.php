<?php declare(strict_types=1);

namespace App\Bitrix24Client\Collections;

use App\Bitrix24Client\Models\Comment;

class CommentsCollection extends AbstractCollection
{
    /**
     * CatalogCollection constructor.
     *
     * @param array[] $array
     */
    public function __construct(iterable $array)
    {
        foreach ($array['result'] as $item) {
            $this->add(Comment::fromArray($item));
        }
    }
}
