<?php declare(strict_types=1);

namespace App\Bitrix24Client\Collections;

use App\Bitrix24Client\Models\Task;

class TasksCollection extends AbstractCollection
{
    /**
     * CatalogCollection constructor.
     *
     * @param array[] $array
     */
    public function __construct(iterable $array)
    {
        foreach ($array['result']['tasks'] as $item) {
            $this->add(Task::fromArray($item));
        }
    }
}
