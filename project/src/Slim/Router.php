<?php declare(strict_types=1);

namespace App\Slim;

use App\Slim\Controllers\CommentsController;
use App\Slim\Controllers\EmployersController;
use App\Slim\Controllers\TasksController;
use Psr\Http\Message\ServerRequestInterface;

class Router
{
    const BASE_PATH       = '';
    const EMPLOYERS_ACTION = EmployersController::class . ':employersAction';
    const TASKS_ACTION = TasksController::class . ':tasksAction';
    const COMMENTS_ACTION = CommentsController::class . ':commentsAction';

    const QUERY_PARAM_EMPLOYEE_ID = "employeeId";
    const QUERY_PARAM_TASK_ID = "taskId";
    
    public static function urlToEmployees()
    {
        return sprintf("%s/", self::BASE_PATH);
    }

    public static function urlToTasks(string $employeeId): string
    {
        return sprintf("%s/%s/tasks/%s", self::BASE_PATH, $employeeId, '?naked=1');
    }

    public static function getEmployeeId(ServerRequestInterface $request, array $args): string
    {
        return isset($args[Router::QUERY_PARAM_EMPLOYEE_ID]) ? $args[Router::QUERY_PARAM_EMPLOYEE_ID] : '';
    }

    public static function getTaskId(ServerRequestInterface $request, array $args): string
    {
        return isset($args[Router::QUERY_PARAM_TASK_ID]) ? $args[Router::QUERY_PARAM_TASK_ID] : '';
    }

    // public static function urlToCars(string $catalogId, string $modelId, string $state = ""): string
    // {
    //     $q = http_build_query([
    //         self::QUERY_PARAM_MODEL_ID => $modelId,
    //         self::QUERY_PARAM_STATE => $state,
    //     ]);
    //     return sprintf("%s/catalogs/%s/cars/?%s", self::BASE_PATH, $catalogId, $q);
    // }

    // public static function urlToFilters($catalogId, $modelId, $state): string
    // {
    //     $q = http_build_query([
    //         self::QUERY_PARAM_MODEL_ID => $modelId,
    //         self::QUERY_PARAM_STATE => $state,
    //     ]);
    //     return sprintf("%s/catalogs/%s/filters/?%s", self::BASE_PATH, $catalogId, $q);
    // }

    // public static function urlToGroups(string $catalogId, string $carId, string $groupPath = "", string $criteria = ""): string
    // {
    //     $q = http_build_query([
    //         self::QUERY_PARAM_CAR_ID => $carId,
    //         self::QUERY_PARAM_GROUP_PATH => $groupPath,
    //         self::QUERY_PARAM_CRITERIA => $criteria,
    //     ]);
    //     return sprintf("%s/catalogs/%s/groups/?%s", self::BASE_PATH, $catalogId, $q);
    // }

    // public static function urlToParts(string $catalogId, string $carId, string $groupPath = "", string $criteria = "")
    // {
    //     $q = http_build_query([
    //         self::QUERY_PARAM_CAR_ID => $carId,
    //         self::QUERY_PARAM_GROUP_PATH => $groupPath,
    //         self::QUERY_PARAM_CRITERIA => $criteria,
    //     ]);
    //     return sprintf("%s/catalogs/%s/parts/?%s", self::BASE_PATH, $catalogId, $q);
    // }

    // public static function urlToSchema($catalogId, $carId, $groupPath, $criteria = "")
    // {
    //     $q = http_build_query([
    //         self::QUERY_PARAM_CAR_ID => $carId,
    //         self::QUERY_PARAM_GROUP_PATH => $groupPath,
    //         self::QUERY_PARAM_CRITERIA => $criteria,
    //     ]);
    //     return sprintf("%s/catalogs/%s/schema/?%s", self::BASE_PATH, $catalogId, $q);
    // }

    // public static function urlToVin()
    // {
    //     return sprintf("%s/vin/", self::BASE_PATH);
    // }

    // public static function getCarId(ServerRequestInterface $request, array $args): ?string
    // {
    //     $q = $request->getQueryParams();
    //     if (isset($q[self::QUERY_PARAM_CAR_ID])) {
    //         return $q[self::QUERY_PARAM_CAR_ID];
    //     }
    //     return "";
    // }

    // /**
    //  * @param ServerRequestInterface $request
    //  *
    //  * @param array                  $args
    //  *
    //  * @return string
    //  */
    // public static function getVin(ServerRequestInterface $request, array $args): string
    // {
    //     $queryParams = $request->getQueryParams();
    //     return isset($queryParams[Router::QUERY_PARAM_VIN]) ? $queryParams[Router::QUERY_PARAM_VIN] : '';
    // }

    // public static function getCriteria(ServerRequestInterface $request, array $args)
    // {
    //     $queryParams = $request->getQueryParams();
    //     return isset($queryParams[Router::QUERY_PARAM_CRITERIA]) ? $queryParams[Router::QUERY_PARAM_CRITERIA] : '';
    // }

    // public static function getCatalogId(ServerRequestInterface $request, array $args): string
    // {
    //     return isset($args[Router::QUERY_PARAM_CATALOG_ID]) ? $args[Router::QUERY_PARAM_CATALOG_ID] : '';
    // }

    // public static function getModelId(ServerRequestInterface $request, array $args): string
    // {
    //     $queryParams = $request->getQueryParams();
    //     return isset($queryParams[Router::QUERY_PARAM_MODEL_ID]) ? $queryParams[Router::QUERY_PARAM_MODEL_ID] : '';
    // }

    // public static function getFilterState(ServerRequestInterface $request, array $args): string
    // {
    //     $queryParams = $request->getQueryParams();
    //     return isset($queryParams[Router::QUERY_PARAM_STATE]) ? $queryParams[Router::QUERY_PARAM_STATE] : '';
    // }

    // public static function getGroupPath(ServerRequestInterface $request, array $args): string
    // {
    //     $queryParams = $request->getQueryParams();
    //     return isset($queryParams[Router::QUERY_PARAM_GROUP_PATH]) ? $queryParams[Router::QUERY_PARAM_GROUP_PATH] : '';
    // }
}
