<?php declare(strict_types=1);

namespace App\Slim\Views;

use App\Slim\Router;

class LayoutView
{
    private $title = '';
    private $h1 = '';
    private $breads = [
        ["url" => "/", "name" => "Главная"],
    ];

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function setBreads(array $breads): self
    {
        $this->breads = $breads;
        return $this;
    }

    public function setH1(string $h1): self
    {
        $this->h1 = $h1;
        return $this;
    }

    public function getBreads(): array
    {
        return $this->breads;
    }

    public function h1(): string
    {
        return $this->h1;
    }

    public function title(): string
    {
        return $this->title;
    }
}
