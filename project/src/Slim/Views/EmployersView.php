<?php declare(strict_types=1);

namespace App\Slim\Views;

use App\Bitrix24Client\Models\Employer;
use App\Bitrix24Client\Collections\EmployersCollection;
use App\Slim\Router;

class EmployersView extends LayoutView
{
    /**
     * @var EmployersCollection|Employer[]
     */
    public $employers;

    public function __construct(EmployersCollection $employers)
    {
        $this->employers = $employers;
    }

    /**
     * Catalogs grouped by first letter
     *
     * @return iterable|Employer[]
     */
    public function groupedEmployers(): iterable
    {
        return $this->employers->sortAndGroup(
            function (Employer $a, Employer $b) {
                return $a->name <=> $b->name;
            },
            function (Employer $employer) {
                return $employer->name[0];
            }
        );
    }

    /**
     * Get url to models page
     *
     * @param string $employeeId
     *
     * @return string
     */
    public function urlToTasks(string $employeeId): string
    {
        return Router::urlToTasks($employeeId);
    }
}
