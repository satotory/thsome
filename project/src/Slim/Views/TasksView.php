<?php declare(strict_types=1);

namespace App\Slim\Views;

use App\Bitrix24Client\Collections\TasksCollection;
use App\Bitrix24Client\Models\Task;

class TasksView extends LayoutView
{
    /**
     * @var TasksCollection|Task[]
     */
    public $tasks;

    /**
     * @var string
     */
    public $employeeId;

    /** @var string */
    public $subdomain;

    public function __construct(TasksCollection $tasks, string $employeeId)
    {
        $this->tasks = $tasks;
        $this->employeeId = $employeeId;
    }

    /**
     * @return TasksView
     */
    public function setSubdomain(string $subdomain)
    {
        $this->subdomain = $subdomain;

        return $this;
    }

    /** 
     * @return string
     */
    public function getSubdomain()
    {
        return $this->subdomain;
    }

    /**
     * Catalogs grouped by first letter
     *
     * @return iterable|Task[]
     */
    public function groupedTasks(): iterable
    {
        return $this->tasks->sortAndGroup(
            function (Task $a, Task $b) {
                return $a->name <=> $b->name;
            },
            function (Task $task) {
                return $task->name[0];
            }
        );
    }
}
