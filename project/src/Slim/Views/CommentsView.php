<?php declare(strict_types=1);

namespace App\Slim\Views;

use App\Bitrix24Client\Collections\CommentsCollection;
use App\Bitrix24Client\Models\Comment;

class CommentsView extends LayoutView
{
    /**
     * @var CommentsCollection|Comment[]
     */
    public $comments;

    public function __construct(CommentsCollection $comments)
    {
        $this->comments = $comments;
    }
}
