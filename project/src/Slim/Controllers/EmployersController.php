<?php declare(strict_types=1);

namespace App\Slim\Controllers;

use App\Bitrix24Client\ClientException;
use App\Slim\Views\EmployersView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class EmployersController extends AbstractController
{
    /**
     * Show list of available catalogs
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param array                  $args
     *
     * @return ResponseInterface
     * @throws ClientException
     */
    public function employersAction(
        ServerRequestInterface $request, 
        ResponseInterface $response, 
        array $args
    ): ResponseInterface {
        $b24Client = $this->getClient();
        $catalogs = $b24Client->getEmployers();

        $view = new EmployersView($catalogs);

        // Render index view
        return $this->render($response, 'employers.phtml', $view);
    }
}
