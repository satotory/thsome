<?php declare(strict_types=1);

namespace App\Slim\Controllers;

use App\Bitrix24Client\Client;
use App\Slim\Views\LayoutView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Views\PhpRenderer;

abstract class AbstractController
{
    private $render;
    private $b24Client;

    public function __construct(PhpRenderer $render, Client $b24Client = null)
    {
        $this->render   = $render;
        $this->b24Client = $b24Client;
    }

    public function getClient(): Client
    {
        return $this->b24Client;
    }

    public function render(ResponseInterface $response, string $string, LayoutView $view, bool $naked = false): ResponseInterface
    {
        if ($naked) {
            $this->render->setLayout('naked.layout.phtml');
        }
        return $this->render->render($response, $string, ['view' => $view]);
    }

    public function errorNotFound(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        throw new HttpNotFoundException($request, $response->getBody());
    }
}
