<?php declare(strict_types=1);

namespace App\Slim\Controllers;

use App\Bitrix24Client\ClientException;
use App\Slim\Router;
use App\Slim\Views\EmployersView;
use App\Slim\Views\TasksView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class TasksController extends AbstractController
{
    /**
     * Show list of available catalogs
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param array                  $args
     *
     * @return ResponseInterface
     * @throws ClientException
     */
    public function tasksAction(
        ServerRequestInterface $request, 
        ResponseInterface $response, 
        array $args
    ): ResponseInterface {
        $b24Client= $this->getClient();
        $employeeId = Router::getEmployeeId($request, $args);
        $tasks    = $b24Client->getTasks($employeeId);

        $view = new TasksView($tasks, $employeeId);

        $view->setSubdomain($b24Client->getOptions()['subdomain']);

        $naked = !!isset($request->getQueryParams()['naked']);
        // Render index view
        return $this->render($response, 'tasks.phtml', $view, $naked);
    }
}
