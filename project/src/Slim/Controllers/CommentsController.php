<?php declare(strict_types=1);

namespace App\Slim\Controllers;

use App\Bitrix24Client\ClientException;
use App\Slim\Router;
use App\Slim\Views\CommentsView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class CommentsController extends AbstractController
{
    /**
     * Show list of available catalogs
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param array                  $args
     *
     * @return ResponseInterface
     * @throws ClientException
     */
    public function commentsAction(
        ServerRequestInterface $request, 
        ResponseInterface $response, 
        array $args
    ): ResponseInterface {
        $b24Client= $this->getClient();
        $employeeId = Router::getTaskId($request, $args);
        $tasks    = $b24Client->getComments($employeeId);

        $view = new CommentsView($tasks);

        $naked = !!isset($request->getQueryParams()['naked']);
        // Render index view
        return $this->render($response, 'comments.phtml', $view, $naked);
    }
}
